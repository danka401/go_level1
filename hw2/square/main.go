package main

import "fmt"

func main() {
	var width int
	for {
		fmt.Println("Введите ширину прямоугольника")
		_, _ = fmt.Scanf("%d\n", &width)
		if width <= 0 {
			fmt.Print("Число долно быть больше нуля. ")
		} else {
			break
		}
	}

	var height int
	for {
		fmt.Println("Введите высоту прямоугольника")
		_, _ = fmt.Scanf("%d\n", &height)
		if height <= 0 {
			fmt.Println("Число долно быть больше нуля")
		} else {
			break
		}
	}

	fmt.Printf("Площадь прямоугольника равна %d\n", width*height)
}
